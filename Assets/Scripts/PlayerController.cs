﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum PlayerState
{
	Idle,
	Walking,
	InAir
}

public class PlayerController : CharacterCore {
	public AudioClip jumpSound, slideSound, winSound;
	public Texture2D scoreOn, scoreOff;
	private bool isSliding = false;
	private float lastSlideSound = 0;
	private bool paused = false;

	[HideInInspector]
	public Vector2 velocity = new Vector2(0, 0);

	[HideInInspector]
	public float horizontal_momentum = 0f;

	private AudioSource slideAudioSource;
	private float deathTime = -1f;
	private Vector2 deathPos;
	private float jumpBuffer = 0f;
	private float jumpStart = 0f;
	private float wallJumpStart = 0f;
	private int wallJumpDir = 0;
	private bool jumping = false;
	private PlayerState playerState = PlayerState.InAir;
	private float blockedTime = 0f;
	private int coins = 0;
	private int score = 0;
	private int lives = 3;
	private float levelTimer = -1f;

	public int HorizontalDirection
	{
		get {
			float raw = Input.GetAxisRaw("Horizontal");
			if (Mathf.Abs(raw) < 0.2) return 0;
			return raw < 0 ? -1 : 1;
		}
	}

	public Vector2 FindStartPoint()
	{
		Vector2 point = Vector2.zero;
		for (int attempts = 0; attempts < 10; attempts++) {
			point.x = Mathf.Floor(UnityEngine.Random.value * TileMap.MAP_WIDTH / 2 + TileMap.MAP_WIDTH / 4) + 0.5f;
			for (point.y = 0; point.y < TileMap.MAP_HEIGHT; point.y++) {
				if (tileMap.IsSolid(point.x, point.y + 1)) {
					return point;
				}
			}
		}
		point.y = Mathf.Floor(UnityEngine.Random.value * TileMap.MAP_HEIGHT / 2 + TileMap.MAP_HEIGHT / 4);
		return point;
	}

	public override void Start()
	{
		base.Start();
		origin = FindStartPoint();
		slideAudioSource = gameObject.AddComponent<AudioSource>();
		slideAudioSource.clip = slideSound;
		slideAudioSource.loop = true;
	}

	public void Update()
	{
		if (levelTimer > Time.time) {
			float accel = (2.0f - (levelTimer - Time.time)) * 30;
			origin = new Vector2(origin.x, origin.y - Time.deltaTime * accel);
			SpriteAnimationManager sam = gameObject.GetComponent<SpriteAnimationManager>();
			sam.SwitchAnimation("WallClingToward", true);
			return;
		} else if (levelTimer > 0) {
			Debug.Log("New Level");
			tileMap.NewLevel(false);
			levelTimer = -1;
			coins = 0;
			velocity = Vector2.zero;
			origin = FindStartPoint();
		}
		if (lives == 0 && deathTime < Time.time - 1.0f && Input.anyKeyDown) {
			tileMap.NewLevel(true);
			velocity = Vector2.zero;
			coins = 0;
			score = 0;
			lives = 3;
			blockedTime = 0;
			GetComponent<SpriteRenderer>().color = Color.white;
			deathTime = -1f;
			origin = FindStartPoint();
		} else if (Input.GetButtonDown("Pause") || (paused && Input.anyKeyDown)) {
			paused = !paused;
			Time.timeScale = paused ? 0 : 1;
		}
		if (paused) {
			return;
		}
		if (isSliding) {
			if (lastSlideSound == 0) {
				slideAudioSource.Play();
				lastSlideSound = Time.time;
			}
			slideAudioSource.volume = Mathf.Clamp((Time.time - lastSlideSound) * 5.0f, 0f, 0.5f);
		} else {
			lastSlideSound = 0;
			if (slideAudioSource.volume > 0.01f) {
				slideAudioSource.volume /= 2f;
			} else {
				slideAudioSource.Stop();
			}
		}
		if (deathTime > 0) {
			float elapsed = Time.time - deathTime;
			origin = new Vector2(origin.x, deathPos.y + (elapsed * elapsed * GRAVITY) - (elapsed * jumpPower * 2));
			if (origin.y > TileMap.MAP_HEIGHT + 5) {
				blockedTime = 0;
				GetComponent<SpriteRenderer>().color = Color.white;
				if (lives > 0) {
					deathTime = -1f;
					origin = FindStartPoint();
				}
			}
			return;
		}
		DoMovement ();
		if (origin.y > TileMap.MAP_HEIGHT + 1) {
			blockedTime = 2.0f;
		} else if (tileMap.IsSolid(midpoint.x, midpoint.y)) {
			blockedTime += Time.deltaTime;
			GetComponent<SpriteRenderer>().color = new Color(1.0f - blockedTime, 1.0f - blockedTime, 1.0f - blockedTime);
		} else if (blockedTime > 0) {
			GetComponent<SpriteRenderer>().color = Color.white;
			blockedTime = 0f;
		}
		if (blockedTime > 1.0f) {
			deathTime = Time.time;
			deathPos = origin;
			gameObject.GetComponent<SpriteAnimationManager>().SwitchAnimation("Ouch", true);
			Kill(false);
			--lives;
		}
	}

	private float lastHeight = 0;
	public void LateUpdate()
	{
		Vector3 cameraPos = deathTime > 0 ? MapToWorldPoint(deathPos) : transform.position;
		if (Screen.height != lastHeight) {
			lastHeight = Screen.height;
			float divisor = 0f;
			float vBlocks;
			do {
				divisor += 1f;
				vBlocks = Mathf.Floor(Screen.height / 16.0f / divisor);
			} while (vBlocks > 15);
			Camera.main.orthographicSize = Screen.height / 16.0f / divisor / 2f;
		}
		float halfWidth = Camera.main.orthographicSize / Screen.height * Screen.width;
		float cameraX = Mathf.Clamp(cameraPos.x, halfWidth, TileMap.MAP_WIDTH - halfWidth);
		float cameraY = Mathf.Clamp(cameraPos.y, Camera.main.orthographicSize - 2, TileMap.MAP_HEIGHT + halfWidth);
		Camera.main.transform.position = new Vector3(cameraX, cameraY, -1);
	}

	private bool ShouldStartJump()
	{
		if (!IsGrounded() && wallCling == 0) {
			return false;
		}
		return (Time.time - jumpBuffer) < 0.05;
	}

	private void StartJump()
	{
		audioSource.PlayOneShot(jumpSound);

		if (!IsGrounded()) {
			if (ledgeGrab == Vector2.zero || ledgeGrab.y > origin.y || (HorizontalDirection != 0 && HorizontalDirection != wallCling))
			{
				wallJumpStart = Time.time;
				wallJumpDir = -wallCling;
				horizontal_momentum = -wallCling * speed * 0.6f;
				velocity.x = horizontal_momentum;
				velocity.y = -jumpPower * 0.8f;
			}
			else
			{
				horizontal_momentum = -wallCling * 2;
				velocity.x = horizontal_momentum;
				velocity.y = -6;
			}
		} else {
			jumpStart = Time.time;
			velocity.y = -jumpPower;
		}
		isGrounded = false;
		jumpBuffer = 0f;
		jumping = true;
		playerState = PlayerState.InAir;
	}

	public void DoMovement()
	{
		bool wasGrounded = isGrounded;
		int xAxis = HorizontalDirection;

		if (Input.GetButtonDown("Jump"))
		{
			jumpBuffer = Time.time;
		}
		jumping = jumping && Input.GetButton ("Jump");


		if (IsGrounded ())
			playerState = PlayerState.Idle;

		switch (playerState)
		{
			case PlayerState.Walking:
			case PlayerState.Idle:
				DoGrounded ();
				break;
			case PlayerState.InAir:
				DoAir ();
				break;
		}

		velocity = Move (velocity);
		isSliding = velocity.y > 0 && wallCling != 0 && !isGrounded;

		SpriteAnimationManager sam = gameObject.GetComponent<SpriteAnimationManager>();
		if (!wasGrounded && isGrounded) {
			sam.PlayOnce("Landing");
		} else if (wasGrounded && !isGrounded) {
			sam.PlayOnce("JumpStart");
		} else if (playerState == PlayerState.Walking) {
			sam.SwitchAnimation("Walking", true);
		} else if (playerState == PlayerState.Idle) {
			sam.SwitchAnimation("Idle", true);
		} else if (wallCling != 0 && velocity.y >= 0) {
			if ((xAxis == 0 || wallCling == xAxis) && ledgeGrab != Vector2.zero) {
				sam.SwitchAnimation("WallClingToward", true);
			} else {
				sam.SwitchAnimation("WallCling", true);
			}
		} else if (Mathf.Abs(velocity.y) < 1.0) {
			sam.SwitchAnimation("JumpIdle", true);
		} else if (velocity.y < 0) {
			sam.SwitchAnimation("JumpUp", true);
		} else if (velocity.y > 0) {
			sam.SwitchAnimation("JumpDown", true);
		}
		if (Time.time - jumpStart < 0.3 || Time.time - wallJumpStart < 0.3) {
			if (Mathf.Abs(velocity.x) > 0.2) {
				GetComponent<SpriteRenderer>().flipX = (velocity.x < 0);
			}
		} else if (xAxis != 0 && ledgeGrab == Vector2.zero) {
			GetComponent<SpriteRenderer>().flipX = (xAxis < 0);
		}
	}


	private void DoGrounded()
	{
		horizontal_momentum = Approach (horizontal_momentum, (Input.GetAxisRaw ("Horizontal") * speed), 0.4f);
		velocity.x = horizontal_momentum;

		if (ShouldStartJump()) {
			StartJump();
		} else {
			velocity.y += GRAVITY * Time.deltaTime;
			if (velocity.y > 10) velocity.y = 10;
		}

		playerState = (Mathf.Abs (Input.GetAxis ("Horizontal")) >= 0.2f && velocity.x != 0) ? PlayerState.Walking : PlayerState.Idle;

		if (!IsGrounded ())
			playerState = PlayerState.InAir;
	}


	private void DoAir()
	{
		if (ledgeGrab == Vector2.zero || ledgeGrab.y > origin.y) {
			if (!jumping && velocity.y < 0)
			{
				velocity.y += 2.5f * GRAVITY * Time.deltaTime;
			}
			velocity.y += GRAVITY * Time.deltaTime;

			if (velocity.y > 10) velocity.y = 10;
		}

		if (ShouldStartJump()) {
			StartJump();
		}

		if (wallCling != 0 && wallCling == Mathf.Sign(Input.GetAxisRaw("Horizontal"))) {
			velocity.y = Approach (velocity.y, Mathf.Lerp(velocity.y, Mathf.Clamp (velocity.y, -10, 2), Mathf.Abs(horizontal_momentum-velocity.x)), 0.7f);
		}

		if (ledgeGrab == Vector2.zero) {
			horizontal_momentum = Approach (horizontal_momentum, Mathf.Lerp (wallJumpDir * speed, Input.GetAxisRaw ("Horizontal") * speed, Mathf.Pow ((Time.time - wallJumpStart) / 0.5f, 2)), 0.2f);
		} else if (ledgeGrab.y <= origin.y) {
			origin = ledgeGrab;
			if (!jumping) velocity.y = 0;
			if (wallCling != 0) {
				GetComponent<SpriteRenderer>().flipX = (wallCling < 0);
			}

			if ((Input.GetAxisRaw ("Vertical") < 0 && Input.GetButtonDown("Vertical")))
			{
				horizontal_momentum = -wallCling * 2;
				velocity.x = horizontal_momentum;
			}
		}
		velocity.x = horizontal_momentum;
	}

	public void OnMapUpdated()
	{
		if (deathTime > 0 || levelTimer > 0) return;

		Vector2 midpoint = this.midpoint;
		if (tileMap.IsSolid(midpoint.x, midpoint.y)) {
			Vector2 target = origin;
			if (!tileMap.IsSolid(target.x, midpoint.y - 1)) {
				target.y -= 1;
			} else if (!tileMap.IsSolid(target.x - 1, midpoint.y)) {
				target.x -= 1;
			} else if (!tileMap.IsSolid(target.x + 1, midpoint.y)) {
				target.x += 1;
			} else if (!tileMap.IsSolid(target.x, midpoint.y + 1)) {
				target.y += 1;
			}
			origin = target;
		}
	}

	public new void OnCollisionEnter2D(Collision2D coll)
	{
		if (deathTime > 0 || levelTimer > 0) return;

		if (coll.gameObject.tag == "Collectible") {
			CharacterCore obj = coll.gameObject.GetComponent<CharacterCore>();
			obj.Kill(true);
			coins++;
			score += 100;
			if (coins > 15) {
				levelTimer = Time.time + 2;
				audioSource.PlayOneShot(winSound);
			}
		}
	}

	public void OnGUI()
	{
		GUIStyle boxStyle = new GUIStyle(GUI.skin.box);
		boxStyle.fontSize = 12;
		boxStyle.alignment = TextAnchor.MiddleLeft;

		GUIStyle labelStyle = new GUIStyle(GUI.skin.label);
		labelStyle.fontSize = 32;
		labelStyle.alignment = TextAnchor.MiddleCenter;

		GUIStyle listStyle = new GUIStyle(labelStyle);
		listStyle.fontSize = 24;
		listStyle.alignment = TextAnchor.UpperLeft;

		GUI.DrawTexture(new Rect(16, 2, -14, 14), scoreOff);
		for (int i = 15; i >= 0; i--) {
			GUI.DrawTexture(new Rect(2 + 8 * i, 2, 14, 14), coins > i ? scoreOn : scoreOff);
		}

		Rect currentScore = new Rect(140, 1, 100, 16);
		GUI.Box (currentScore, " ", boxStyle);
		GUI.Box (currentScore, "Score: " + score, boxStyle);

		Rect currentLives = new Rect(244, 1, 100, 16);
		GUI.Box (currentLives, " ", boxStyle);
		GUI.Box (currentLives, "Lives: " + lives, boxStyle);

		if (lives == 0) {
			Rect pauseBox = new Rect(Screen.width * 0.3f, Screen.height * 0.3f, Screen.width * 0.4f, Screen.height * 0.4f);
			GUI.Box (pauseBox, " ", boxStyle);
			GUI.Box (pauseBox, " ", boxStyle);
			if ((int)(System.DateTime.Now.Millisecond) < 500) {
				GUI.Label (pauseBox, "GAME OVER\n\nPress Any Key", labelStyle);
			}
			return;
		} else if (paused) {
			Rect pauseBox = new Rect(Screen.width * 0.3f, Screen.height * 0.3f, Screen.width * 0.4f, Screen.height * 0.4f);
			GUI.Box (pauseBox, " ", boxStyle);
			GUI.Box (pauseBox, " ", boxStyle);
			if ((int)(System.DateTime.Now.Millisecond) < 500) {
				GUI.Label (pauseBox, "PAUSED\n\nPress Any Key", labelStyle);
			}
			return;
		}
	}
}
