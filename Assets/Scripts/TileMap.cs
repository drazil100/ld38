using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

public class TileMap : MonoBehaviour {
	private static Vector3 LAYER_1 = new Vector3(0, 0, -0.2f);
	private static Vector3 LAYER_2 = new Vector3(0, 0, -0.3f);
	private static Vector3 LAYER_3 = new Vector3(0, 0, -0.4f);

	public const int MAP_WIDTH = 32;
	public const int MAP_HEIGHT = 32;
	public const float DEPTH_SCALE = 0.8f;

	public CharacterCore coinPrefab;
	public AudioClip quakeSound;
	public uint seed;

	public bool[,] tileData;
	public bool[,] nextTileData;
	public List<bool[,]> layers;
	public Material material;
	private Material[] layerMaterial;
	private float depth;
	private int level;
	private Mesh nextMesh;
	private Mesh persistMesh;
	private Mesh currentMesh;
	[HideInInspector]
	public AudioSource audioSource;

	private bool[,] postTileData = null;

	private static float tileSize = 1f / 6f;
	private static Vector2 TileToUV(int x, int y) {
		return new Vector2(x * tileSize, (5 - y) * tileSize);
	}
	private static Vector2[] tileUV = {
		/* .0. 0*0 .0. */ TileToUV(4, 3),
		/* .0. 0*0 .1. */ TileToUV(3, 3),
		/* .0. 0*1 .0. */ TileToUV(0, 3),
		/* .0. 0*1 .1. */ TileToUV(0, 0),
		/* .0. 1*0 .0. */ TileToUV(2, 3),
		/* .0. 1*0 .1. */ TileToUV(2, 0),
		/* .0. 1*1 .0. */ TileToUV(1, 3),
		/* .0. 1*1 .1. */ TileToUV(1, 0),
		/* .1. 0*0 .0. */ TileToUV(3, 5),
		/* .1. 0*0 .1. */ TileToUV(3, 4),
		/* .1. 0*1 .0. */ TileToUV(0, 2),
		/* .1. 0*1 .1. */ TileToUV(0, 1),
		/* .1. 1*0 .0. */ TileToUV(2, 2),
		/* .1. 1*0 .1. */ TileToUV(2, 1),
		/* .1. 1*1 .0. */ TileToUV(1, 2),
		/* .1. 1*1 .1. */ TileToUV(1, 1)
	};

	private double ValueAt(float x, float y, float depth) {
		double noise = NoiseField.noise(x, y, depth / DEPTH_SCALE, seed, 5.0f) + 0.2;
		noise *= Mathf.Pow(0.99f, depth - 1);
		noise -= 0.2;
		return noise;
	}

	public void GenerateTiles(float depth) {
		float depthPower = 3f * Mathf.Pow(0.05f, 1.0f / (depth + 3.0f));
		for (int y = 0; y < MAP_HEIGHT; y++) {
			float threshold = 0.2f - (float)Mathf.Pow((float)y / MAP_HEIGHT * 0.4f, depthPower) / 1.0f;
			for (int x = 0; x < MAP_WIDTH; x++) {
				if (postTileData == null || tileData[x, y] != postTileData[x, y]) {
					tileData[x, y] = ValueAt(x, y, depth) > threshold;
				}
				nextTileData[x, y] = ValueAt(x, y, depth + 1) > threshold;
			}
		}

		PopulateMesh(currentMesh, 0);
		PopulateMesh(nextMesh, 1);
		PopulateMesh(persistMesh, 2);

		PlayerController player = GameObject.FindWithTag("Player").GetComponent<PlayerController>();
		player.OnMapUpdated();
	}

	public void Awake()
	{
		seed = (uint)(UnityEngine.Random.value * 1000000f);
		audioSource = gameObject.AddComponent<AudioSource>();
		audioSource.clip = quakeSound;
		audioSource.pitch = 0.7f;
		currentMesh = new Mesh();
		nextMesh = new Mesh();
		persistMesh = new Mesh();
		tileData = new bool[MAP_WIDTH, MAP_HEIGHT];
		nextTileData = new bool[MAP_WIDTH, MAP_HEIGHT];
		layers = new List<bool[,]>();
		layers.Add(tileData);
		layers.Add(nextTileData);
		layers.Add(tileData);
		layerMaterial = new Material[3];
		layerMaterial[0] = new Material(material);
		layerMaterial[0].SetColor("_Color", new Color(1, 1, 1, 0.7f));
		layerMaterial[1] = new Material(material);
		layerMaterial[1].SetColor("_Color", new Color(0.3f, 0.6f, 1.0f, 0.3f));
		layerMaterial[2] = new Material(material);
		layerMaterial[2].SetColor("_Color", new Color(1f, 1f, 1f, 1f));

		depth = 0f;
		GenerateTiles(depth);

		GeneratePickups();
	}

	public void NewLevel(bool startOver) {
		if (startOver) {
			level = 0;
		} else {
			level++;
		}
		seed = (uint)(UnityEngine.Random.value * 1000000f);
		depth = level * 3;
		GenerateTiles(depth);
		GeneratePickups();
	}

	public void GeneratePickups() {
		int hBlock = MAP_WIDTH / 4;
		int vBlock = MAP_HEIGHT / 4;
		for (int y = 0; y < MAP_HEIGHT; y += vBlock) {
			for (int x = 0; x < MAP_WIDTH; x += hBlock) {
				CharacterCore coin = UnityEngine.Object.Instantiate(coinPrefab).GetComponent<CharacterCore>();
				coin.origin = new Vector2(
					Mathf.Floor(UnityEngine.Random.value * hBlock) + x + 0.5f,
					Mathf.Floor(UnityEngine.Random.value * vBlock) + y
				);
			}
		}
	}

	public bool IsSolid(float x, float y, int layer = 0)
	{
		if (x < 0 || x >= MAP_WIDTH) {
			return true;
		}
		if (y < 0 || y >= MAP_HEIGHT) {
			return false;
		}
		return layers[layer][(int)x, (int)y];
	}

	public void PopulateMesh(Mesh mesh, int layer)
	{
		bool[,] data = layers[layer];
		mesh.Clear();
		List<Vector3> vertices = new List<Vector3>();
		List<Vector2> uv = new List<Vector2>();
		List<int> triangles = new List<int>();
		int triangleBase = 0;
		for (int y = 0; y < MAP_HEIGHT; y++) {
			for (int x = 0; x < MAP_WIDTH; x++) {
				if (!data[x, y]) continue;
				if (layer == 2 && !nextTileData[x, y]) continue;
				vertices.Add(new Vector3(x, 30 - y, 0));
				vertices.Add(new Vector3(x + 1, 30 - y, 0));
				vertices.Add(new Vector3(x + 1, 30 - y - 1, 0));
				vertices.Add(new Vector3(x, 30 - y - 1, 0));
				int bits = 0;
				if (IsSolid(x, y-1, layer)) bits |= 8;
				if (IsSolid(x-1, y, layer)) bits |= 4;
				if (IsSolid(x+1, y, layer)) bits |= 2;
				if (IsSolid(x, y+1, layer)) bits |= 1;
				Vector2 uvPos = tileUV[bits];
				uv.Add(new Vector2(uvPos.x,            uvPos.y + tileSize));
				uv.Add(new Vector2(uvPos.x + tileSize, uvPos.y + tileSize));
				uv.Add(new Vector2(uvPos.x + tileSize, uvPos.y));
				uv.Add(new Vector2(uvPos.x,            uvPos.y));
				triangles.Add(triangleBase);
				triangles.Add(triangleBase+1);
				triangles.Add(triangleBase+2);
				triangles.Add(triangleBase);
				triangles.Add(triangleBase+2);
				triangles.Add(triangleBase+3);
				triangleBase += 4;
			}
		}
		mesh.vertices = vertices.ToArray();
		mesh.uv = uv.ToArray();
		mesh.triangles = triangles.ToArray();
	}

	public float shiftTimer = 0;
	public void Update() {
		shiftTimer += Time.deltaTime;
		if (shiftTimer > 6.0f) {
			depth++;
			shiftTimer = depth / 10.0f;
			postTileData = null;
			GenerateTiles(depth);
		} else if (shiftTimer > 5.0f) {
			if (postTileData == null) {
				postTileData = nextTileData.Clone() as bool[,];
			}
			GenerateTiles(depth + shiftTimer - 5.0f);
		}
		Vector3 currentPos = LAYER_2;
		if (shiftTimer > 4.0f) {
			if (!audioSource.isPlaying && shiftTimer < 5.0f) audioSource.Play();
			layerMaterial[0].SetColor("_Color", new Color(1, 1, 1, Mathf.Pow(0.5f + Mathf.Abs(5.0f - shiftTimer) / 2f, 0.5f)));
			if (shiftTimer < 5.0f) {
				currentPos += UnityEngine.Random.insideUnitSphere * 0.08f * (0.5f - Mathf.Abs(4.5f - shiftTimer));
			}
		} else {
			layerMaterial[0].SetColor("_Color", Color.white);
		}
		Graphics.DrawMesh(nextMesh, LAYER_1, Quaternion.identity, layerMaterial[1], 2, null, 0, null, false, false, false);
		Graphics.DrawMesh(currentMesh, currentPos, Quaternion.identity, layerMaterial[0], 0, null, 0, null, false, false, false);
		Graphics.DrawMesh(persistMesh, LAYER_3, Quaternion.identity, layerMaterial[2], 1, null, 0, null, false, false, false);
	}
}
